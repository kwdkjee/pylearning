# 字典相关操作
# 创建字典
#my_dict = {1:"python", 2:"lemon"}
#my_dict = dict({1:"python", 2:"lemon"})
user_info = {"name": "小明", "age": 18, "gender": True, "height": 1.75}
# 确认类型是否字典
# print(type(my_dict))

# 1、字典求长度
print(len(user_info))

# 获取某个数据
print(user_info["name"])   # 键不存在报错
print(user_info.get("name"))  # 键不存在不报错
print(user_info.get("motto"))
print(user_info.get("motto", "lemonn"))

# 获取所有keys
print(user_info.keys())
print(list(user_info.keys()))  # 转换成列表的元素

# 获取所有values
print(user_info.values())
print(list(user_info.values()))  # 转换成列表的元素
print(list(user_info.items()))   # 把键转换为元组，把元组当成列表的数据

# # 修改值
# print(id(user_info))
# user_info["name"] = "可优"
# print(id(user_info))
# print(user_info)     # 证明没有创建一个新字典，是在原字典进行修改
#
#
# anthor_info = {"motto": "never stop learning!", ("love", ): "python automated testing"}  # ("love", )是元组
# # 合并字典
# user_info.update(anthor_info)
# print(user_info)
#
# # 删除指定的键值对
# user_info.pop("motto")
# print(user_info)
#
# user_info.popitem()  # 删除最后一个元素
# print(user_info)
#
# user_info.clear()  # 清空字典
# print(user_info)



