# 打印99乘法表

# 先打印九行星

# i = 1
# j = 1
# start = "*"
# while i <= 9:
#     print(start * i)
#     i = i + 1

# i是行数
i = 1
while i <= 9:
    # j是一行的个数
    j = 1
    while j <= i:
        # 只要j <=i ,就会一直在while j <= i这个循环内，不会去到while i <= 9循环
        print("{:d}*{:d}={:d}".format(i, j, i*j), end=" ")
        j += 1
    print()
    i = i + 1


year = int(input("请输入年份："))
# 判断该年份是不是闰年
if year % 4 == 0 and year % 400 == 0:
    print("{}是闰年".format(year))
else:
    print("{}年不是闰年".format(year))
    pass
