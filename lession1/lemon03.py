# 练习序列类型中支持的操作
# 定义一个包含十二生肖的字符串
animal = '猴鸡狗猪鼠牛虎兔龙蛇马羊'

# 1、通过索引取值
print(animal[3])

# 2、切片操作
print(animal[1:-3])

# 成员关系操作

print("猪" in animal)
print("猫" in animal)
print("猫" not in animal)

# 4、连接操作
new_animal = animal + "猫"
print(new_animal)

# 5、字符串的重复操作符
print(animal * 3)

# 支持遍历 就是for循环
for item in animal:
    print(item)

# 7、求长度
print(len(animal))

list1 = list('hello')
print(list1)

gender = input();

print("个人信息展示\n", "性别：{}\n" .format(bool(gender)))

