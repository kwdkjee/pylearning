# 遇到3就不打印
# break
i = 0
while i < 10:
    if i == 3:
        # 跳出if循环
        break
    print(i)
    i += 1
print("继续执行")

# continue
j = 0
while j < 10:
    if j == 3:
        # 跳出if循环
        j += 1
        continue
    print(j)
    j = j + 1

print("继续执行")
