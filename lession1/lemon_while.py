# 打印5遍hello python
# 定义计数器
# i = 1
# # while i <= 5:
# #     print("hello python")
# #     i = i + 1
# #
# # print("循环结束后的i={:d}".format(i))

# 1~100相加的结果
# 1、定义最终结果的变量
result = 0
# 2、定义一个变量来记录循环次数
i = 1

# 3、定义循环条件
while i <=100:
    result = result + i
    i += 1

print("0~100求和结果为：{}".format(result))


# 计算0~100所有偶数累计求和结果
num = 0
i =1
while i <=100:
    if i % 2 ==0:
        num += i
        i += 1
    i +=1
print(num)

