# 列表相关操作
# 定义列表
# a_var = "python自动化测试16班人才辈出"
# a_list = ["可优", 18, a_var, True, None, ["python", "java", "php"]]
# print("源列表：\n", a_list)
#
# # 修改列表
#
# a_list[1] = 17
# print("修改列表元素之后的列表：\n", a_list)
# print("*" * 100)
#
# # 通过切片来赋值
# a_list[3:5] = [1, 0]
# print("通过切片来赋值的列表：\n", a_list)

# 插入元素
# a_list.append("小仙女")
# a_list.append(["apple", "orange", "banana"])
# print("插入元素后的列表", a_list)
# extend 将序列类型数据扩充到列表中
# a_list.extend(["apple", "orange", "banana"])
# a_list.extend('lemon')
# print(a_list)

# insert在指定位置插入数据
# a_list.insert(3, "lemon")
# print(a_list)

# 删除元素
# del a_list[1]
# print(a_list)

# remove删除第一个出现的元素
# a_list.remove(18)
# print(a_list)

# pop默认删除最后一个元素，pop的返回结果是删除的元素
# print(a_list.pop())

# 清空列表
# print(a_list.clear())
# print(a_list)

# count 计数
my_list = list("lemon is very best!")
print(my_list.count("e"))

# 排序
b_list = [10, 1, 3, 8, 20, 4, 0, 90]
b_list.sort()
print(b_list)
c_list = [10, 1, 3, 8, 20, 4, 0, 90]
c_list.sort(reverse=True)
print(c_list)

# 原列表反转排序
d_list = [10, 1, 3, 8, 20, 4, 0, 90]
d_list.reverse()
print(d_list)

# all 列表内所有元素是True,则为True
e_list = [1, 2, 3, 4]
print(all(e_list))
f_list = [1, 2, 3, 0]
print(all(f_list))

# any 列表内只有一个元素是True,则为True 
