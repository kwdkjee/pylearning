

def circle_area(n):
    """
    求圆的面积
    :param n: 圆的半径
    :return: 圆的面积
    """
    return 3.14 * n ** 2


def circle_meter(n):
    """

    :param n: 半径
    :return: 周长
    """
    return 3.14 * 2 * n


def rectangle_area(x, y):
    """

    :param x: 长
    :param y: 宽
    :return: 面积
    """
    return x * y


def rectangle_meter(x, y):
    """

    :param x: 长
    :param y: 宽
    :return: 周长
    """
    return 2 * x + 2 * y


if __name__ == '__main__':
    if circle_area(2) == 12.56:
        print("圆面积计算正确")
    if circle_meter(2) == 12.56:
        print("圆周长计算正确")
    if rectangle_area(2,3) == 6:
        print("长方形面积计算正确")
    if rectangle_meter(2, 3) == 10:
        print("长方形周长计算正确")