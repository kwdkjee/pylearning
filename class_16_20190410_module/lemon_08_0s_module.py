# os模块常见操作
import os
# #
# # # 查看操作系统名称
# # print(os.name)
# #
# # # 查看当前路径
# # print(os.getcwd())
# #
# # # 查看系统环境变量
# # print(os.environ["PATH"])

# 创建文件夹,已经存在会报错,在当前文件的目录创建,mkdir创建一层目录
# os.mkdir('test1')

# 创建嵌套文件夹,多层目录
# os.makedirs("D:\work\pylearning/test1/test2")

# 删除指定目录
#os.rmdir("D:\work\pylearning/test1/test2")
# remove可能能删多级目录


# 列出指定路径下的目录结构
print(os.listdir("D:\work\pylearning\class_16_20190410_module"))

# 返回路径的文件名,basename返回最后一个斜杠后的内容，
print(os.path.basename("D:\work\pylearning\class_16_20190410_module\lemon_08_0s_module.py"))

# dirname是除了最后一个斜杠之前的内容

# exists是判断文件是否存在,
print(os.path.exists("D:\work\pylearning\class_16_20190410_module\lemon_08_0s_module.py"))

# isdir判断是否是目录,isfile判断是否文件
print(os.path.isdir("D:\work\pylearning\class_16_20190410_module\lemon_08_0s_module.py"))
print(os.path.isdir("D:\work\pylearning\class_16_20190410_module"))

# join将两部分进行合并
# print(os.path.join("D:\work\pylearning\class_16_20190410_module", "lemon_08_0s_module.py"))

# split把/最后的分割开
print(os.path.split("D:\work\pylearning\class_16_20190410_module/lemon_08_0s_module.py"))
